import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Ivan", "Ivanov", (byte)23, "Sofia University", 12345);
        System.out.println(student); // Automatically invokes .toString()

        Employee employee = new Employee("Georgi", "Georgiev", (byte)32, "Soft Academy", LocalDate.now());
        System.out.println(employee); // Automatically invokes .toString()
    }
}
