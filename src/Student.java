public class Student extends Person {
    private String university;
    private int facultyNumber;

    public Student(String firstName, String lastName, byte age, String university, int facultyNumber) {
        super(firstName, lastName, age);
        this.university = university;
        this.facultyNumber = facultyNumber;
    }

    public String getUniversity() {
        return this.university;
    }

    public int getFacultyNumber() {
        return this.facultyNumber;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + this.getFacultyNumber() + ", " + this.getUniversity();
    }
}
