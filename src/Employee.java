import java.time.LocalDate;

public class Employee extends Person {
    private String company;
    private LocalDate hireDate;

    public Employee(String firstName, String lastName, byte age, String company, LocalDate hireDate) {
        super(firstName, lastName, age);
        this.company = company;
        this.hireDate = hireDate;
    }

    public String getCompany() {
        return this.company;
    }

    public LocalDate getHireDate() {
        return this.hireDate;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + this.getHireDate() + ", " + this.getCompany();
    }
}
